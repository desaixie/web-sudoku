const express = require("express");
const fs = require('fs');
const path = require("path");
const http = require("http");
const bodyParser = require("body-parser");


const app = express();
const publicPath = path.resolve(__dirname, "public");
app.use(express.static(publicPath));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/input",function(req, res) {
    let difficulty = req.query.difficulty;
    let numbers;
    let answer;
    
    if(difficulty === "demo"){
        let textByLine = fs.readFileSync(difficulty + '.txt').toString().split("\n");
        let textByLine1 = fs.readFileSync(difficulty + 'Answer.txt').toString().split("\n");
        numbers = Array(9);
        answer = Array(9);
        for (let i = 0; i < 9; i++) {
            numbers[i] = Array(9);
            answer[i] = Array(9);
            for (let j = 0; j < 9; j++) {
                numbers[i][j] = Number.parseInt(textByLine[i].charAt(j));
                answer[i][j] = Number.parseInt(textByLine1[i].charAt(j));
            }
        }
    }else{
        let textByLine = fs.readFileSync(difficulty + '.txt').toString().split("\n");
        let textByLine1 = fs.readFileSync(difficulty + 'Answer.txt').toString().split("\n");
        numbers = Array(19);
        answer = Array(19);
        for (let i = 0; i < 19; i++) {
            numbers[i] = Array(9);
            answer[i] = Array(9);
            if(textByLine[i].charAt(0) === "a"){
                continue;
            }
            for (let j = 0; j < 9; j++) {
                numbers[i][j] = Number.parseInt(textByLine[i].charAt(j));
                answer[i][j] = Number.parseInt(textByLine1[i].charAt(j));
            }
        }
    }
    res.writeHead(200, {
        'Content-Type': 'application/json'
    });
    res.end(JSON.stringify({numbers: numbers, answer: answer}));
});
// app.listen(3000);

app.use((req, res) => {
    res.writeHead(404);
    res.end('Not found.');
    console.log('Error: Invalid Request: ' + req.url);
});

const server = http.createServer(app);
server.listen(3000, "localhost", function(){
   console.log("Server running at localhost:3000");
});
