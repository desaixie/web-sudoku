## Sudoku

To start the server, in the root directory, type 
>npm install  
node app.js

Then open [localhost:3000](localhost:3000) in your browser.

[Check the rules of Sudoku here. ](https://www.kristanix.com/sudokuepic/sudoku-rules.php)

To play, toggle a cell that you want to enter a number in. You can enter the number into the toggled
 cell by either click on the numpad or use the keyboard. On keyboard number 0 works the same as 
 the blank button on the numpad.
 
The game finishes when the grid is filled and no rules of Sudoku are violated.
