/*
TODO: Keyboard Input.
 */
const SERVER = "http://localhost:3000";
let toggledCell = -1;
let enteredNumbers = new Array(9);
let numbers;
let answer;
let currentPuzzle = 0;
let difficulty;
let timer;

$(function(){
    selectDifficulty();
    $("button.anotherone")[0].onclick = onClickAnotherOne;
    document.addEventListener("keydown", onKeyStroke);
});

function selectDifficulty(){
    $(".anotherone").css("display", "none");
    $("button.dif")[0].onclick = onClickConfirm;
}

function onClickConfirm(event){
    let radiobtns = $('input[type=radio]');
    let difficulties = ["easy", "medium", "demo"];
    for (let i = 0; i < 3; i++) {
        if(radiobtns[i].checked){
            difficulty = difficulties[i]
        }
    }
    console.log(difficulty);
    $(".dif").css("display", "none");
    $(".anotherone").css("display", "block");
    startPage();
}

function startPage(){
    doAjaxCall("GET", "/input", {difficulty: difficulty}, function(jsobj){
        numbers = jsobj.numbers;
        answer = jsobj.answer;
        if(difficulty === "demo"){
            $("button.anotherone").css("display", "none");
        }else{
            if(currentPuzzle === 0){
                numbers = numbers.slice(0, 9);
                answer = answer.slice(0, 9)
            }else{
                numbers = numbers.slice(10, 19);
                answer = answer.slice(10, 19)
            }
        }
        
        let grid = $('tbody.grid');
        grid.text("");
        for (let i = 0; i < 9; i++) {
            grid.append(`<tr class='grid' data-r=${i}>a</tr>`);
            enteredNumbers[i] = new Array(9);
            for (let j = 0; j < 9; j++) {
                enteredNumbers[i][j] = numbers[i][j];
                if(numbers[i][j] !== 0){
                    grid.children().last().append(`<td class="grid immutable" data-r=${i} data-c=${j}
                            data-num=${numbers[i][j]} style="background-color: #cce5ff">${numbers[i][j]}</td>`);
                }else{
                    grid.children().last().append(`<td class="grid" data-r=${i} data-c=${j} data-num=${numbers[i][j]}></td>`);
                }
                
            }
        }
        $("table.grid")[0].onclick = function(event){onClickGrid(event);};
        
        let options = $("table.options");
        options.children().first().text("");
        for (let i = 0; i < 3; i++) {
            options.children().first().append(`<tr class="options" data-r=${i}></tr>`);
            for (let j = 0; j < 3; j++) {
                let num = i*3 + j + 1;
                options.children().first().children().last().append(`<td class="options" data-r=${i} data-c=${j} data-num=${num}>${num}</td>`)
            }
            
        }
        options.children().first().append("<tr class='options'><td class='options' colspan='3' data-num='-1'></td></tr>");
        options[0].onclick = function(event){ onClickOptions(event); };
        setTimer();
    });
}

// Utility method for encapsulating the jQuery Ajax Call
function doAjaxCall(method, cmd, params, fcn) {
    $.ajax(
            SERVER + cmd,
            {
                type: method,
                headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                },
                processData: true,
                data: params,
                dataType: "json",
                success: function (result) {
                    fcn(result)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Error: " + jqXHR.responseText);
                    alert("Error: " + textStatus);
                    alert("Error: " + errorThrown);
                }
            }
    );
}


function getRegion(row, col){
    if(col < 3){
        if(row < 3){
            return [enteredNumbers[0][0],
            enteredNumbers[0][1],
            enteredNumbers[0][2],
            enteredNumbers[1][0],
            enteredNumbers[1][1],
            enteredNumbers[1][2],
            enteredNumbers[2][0],
            enteredNumbers[2][1],
            enteredNumbers[2][2]];
        }else if(row < 6){
            return [enteredNumbers[0][0],
                enteredNumbers[0][1],
                enteredNumbers[0][2],
                enteredNumbers[1][0],
                enteredNumbers[1][1],
                enteredNumbers[1][2],
                enteredNumbers[2][0],
                enteredNumbers[2][1],
                enteredNumbers[2][2]];
        }else{
            return [enteredNumbers[0][0],
                enteredNumbers[0][1],
                enteredNumbers[0][2],
                enteredNumbers[1][0],
                enteredNumbers[1][1],
                enteredNumbers[1][2],
                enteredNumbers[2][0],
                enteredNumbers[2][1],
                enteredNumbers[2][2]];
        }
    }else if(col < 6){
        if(row < 3){
            return [enteredNumbers[0][3],
                enteredNumbers[0][4],
                enteredNumbers[0][5],
                enteredNumbers[1][3],
                enteredNumbers[1][4],
                enteredNumbers[1][5],
                enteredNumbers[2][3],
                enteredNumbers[2][4],
                enteredNumbers[2][5]];

        }else if(row < 6){
            return [enteredNumbers[3][3],
                enteredNumbers[3][4],
                enteredNumbers[3][5],
                enteredNumbers[4][3],
                enteredNumbers[4][4],
                enteredNumbers[4][5],
                enteredNumbers[5][3],
                enteredNumbers[5][4],
                enteredNumbers[5][5]];
        }else{
            return [enteredNumbers[6][3],
                enteredNumbers[6][4],
                enteredNumbers[6][5],
                enteredNumbers[7][3],
                enteredNumbers[7][4],
                enteredNumbers[7][5],
                enteredNumbers[8][3],
                enteredNumbers[8][4],
                enteredNumbers[8][5]];
        }
        
    }else{
        if(row < 3){
            return [enteredNumbers[6][0],
                enteredNumbers[6][1],
                enteredNumbers[6][2],
                enteredNumbers[7][0],
                enteredNumbers[7][1],
                enteredNumbers[7][2],
                enteredNumbers[8][0],
                enteredNumbers[8][1],
                enteredNumbers[8][2]];
        
        }else if(row < 6){
            return [enteredNumbers[6][3],
                enteredNumbers[6][4],
                enteredNumbers[6][5],
                enteredNumbers[7][3],
                enteredNumbers[7][4],
                enteredNumbers[7][5],
                enteredNumbers[8][3],
                enteredNumbers[8][4],
                enteredNumbers[8][5]];
        }else{
            return [enteredNumbers[6][6],
                enteredNumbers[6][7],
                enteredNumbers[6][8],
                enteredNumbers[7][6],
                enteredNumbers[7][7],
                enteredNumbers[7][8],
                enteredNumbers[8][6],
                enteredNumbers[8][7],
                enteredNumbers[8][8]];
        }
        
    }
}

function onClickGrid(event){
    let clickedCell = $(event.target);
    if(clickedCell.hasClass("immutable")){
    
    }else if(clickedCell[0].toggled){
        clickedCell.css({"background-color": "transparent"});
        clickedCell[0].toggled = false;
        toggledCell = -1;
        // for (let i = 0; i < letters.length; i++) {
        //     if(letters[i].r === row && letters[i].c === col){
        //         letters.splice(i, 1);
        //     }
        // }
    }else{
        clickedCell.css({"background-color": "orange"});
        clickedCell[0].toggled = true;
        if(toggledCell !== -1){
            toggledCell.css({"background-color": "transparent"});
            toggledCell[0].toggled = false;
        }
        toggledCell = clickedCell;
        // letters.push({r:row, c:col});
        setAvailabelOptions(clickedCell);
    }
    
}

function setAvailabelOptions(clickedCell){
    let row = clickedCell[0].dataset.r;
    let col = clickedCell[0].dataset.c;
    let region = getRegion(row, col);
    for (let i = 0; i < 9; i++) {
        if(region[i] !== 0){
            $(`td.options[data-num=${region[i]}]`).css({"background-color":"0x4d4d4d"});
        }
    }
}

// function unsetAvailabelOptions(){
//
// }

function onClickOptions(event){
    if(toggledCell !== -1){
        let row = toggledCell[0].dataset.r;
        let col = toggledCell[0].dataset.c;
        let clickedCell = $(event.target);
        if(clickedCell[0].dataset.num === "-1"){
            enteredNumbers[row][col] = 0;
            toggledCell.css({"background-color":"transparent"});
            toggledCell.text("");
            toggledCell[0].dataset.num = clickedCell.text();
            toggledCell[0].toggled = false;
            toggledCell = -1;
        }else{
            enteredNumbers[row][col] = Number.parseInt(clickedCell.text());
            toggledCell.text(clickedCell.text());
            toggledCell.css({"background-color":"transparent"});
            toggledCell[0].dataset.num = clickedCell.text();
            toggledCell[0].toggled = false;
            toggledCell = -1;
        }
        checkFinish();
    }
    
}

function onKeyStroke(event){
    if(toggledCell !== -1){
        let number = event.keyCode - 48;
        let row = toggledCell[0].dataset.r;
        let col = toggledCell[0].dataset.c;
        if(number < 1){
            enteredNumbers[row][col] = 0;
            toggledCell.css({"background-color":"transparent"});
            toggledCell.text("");
            toggledCell[0].dataset.num = "0";
            toggledCell[0].toggled = false;
            toggledCell = -1;
        }else{
            enteredNumbers[row][col] = number;
            toggledCell.text(number.toString());
            toggledCell.css({"background-color":"transparent"});
            toggledCell[0].dataset.num = number.toString();
            toggledCell[0].toggled = false;
            toggledCell = -1;
        }
        checkFinish();
    }
    
}
function checkFinish(){
    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            if(enteredNumbers[i][j] !== answer[i][j]){
                console.log("not correct:" + i + " " + j);
                console.log(enteredNumbers[i][j]);
                return false;
            }
        }
        
    }
    window.clearInterval(timer);
    window.alert("Congratulations! You solved this Sudoku puzzle in " + $("h3.timer").text() +"!");
    console.log("Congratulations! You solved this Sudoku puzzle in " + $("h3.timer").text() +"!");
    return true;
}

function setTimer(){
    // Update the count down every 1 second
    let start = new Date().getTime();
    timer = setInterval(function() {
        
        // Get today's date and time
        let now = new Date().getTime();
        
        // Find the distance between now and the count down date
        let distance = now - start;
        
        // Time calculations for days, hours, minutes and seconds
        let hours = pad(Math.floor(distance  / (1000 * 60 * 60)), 2);
        let minutes = pad(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)), 2);
        let seconds = pad(Math.floor((distance % (1000 * 60)) / 1000), 2);
        
        // Display the result in the element with id="demo"
        $("h3.timer").text(hours + ":" + minutes + ":" + seconds);
    }, 1000);
}

function pad(num, size) {
    let s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function onClickAnotherOne(){
    if(currentPuzzle === 0){
        currentPuzzle = 1;
    }else{
        currentPuzzle = 0;
    }
    window.clearInterval(timer);
    startPage();
}